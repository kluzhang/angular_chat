
import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { routes } from '../../app/app-routing.module';
@Component({
    selector: 'header-bar',
    template: `
        <div class="wcim__topBar" *ngIf="isShow">
            <div class="inner flexbox flex-alignc">
                <h2 class="barTit flex1"><div class="barCell flexbox flex__direction-column"><em class="clamp1">Angular聊天室</em></div>
                </h2>
                <a class="linkico wcim__ripple-fff" href="javascript:;"><i class="iconfont icon-search"></i></a> <a class="linkico wcim__ripple-fff" href="javascript:;" id="J__topbarAdd"><i class="iconfont icon-tianjia"></i></a>
            </div>
        </div>
    `,
    styles: [``]
})
export class HeaderComponent {
    private isShow: boolean

    constructor(
        private router: Router,
    ) {
        router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                let curPath = event.url.split('/').join('').replace('#', '');
                routes.map((item, index) => {
                    let routePath = item.path.split('/').join('').replace('#', '');
                    if(curPath === routePath){
                        this.isShow = item.data && item.data.showHeader ? true : false
                    }
                })
            }
        });
    }
}
