/*
 *  @tmpl 群信息模板
 */

import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
declare var wcPop: any
@Component({
    selector: 'app-groupinfo',
    template: `
        <div class="wcim__topBar"> <div class="inner flexbox flex-alignc">
                <a class="linkico wcim__ripple-fff" (click)="routeBack()"><i class="iconfont icon-back"></i></a><h2 class="barTit sm flex1">
                    <div class="barCell flexbox flex__direction-column"><em class="clamp1">群信息</em></div> </h2>
            </div>
        </div>
        <div class="wc__ucinfoPanel">
            <div class="wc__ucinfo-qunMember">
                <ul class="clearfix">
                    <li><a routerLink="/contact/uinfo"><img class="uimg" src="../../assets/img/uimg/u__chat-img13.jpg" /><span class="name">炳哥</span></a></li> <li><a routerLink="/contact/uinfo"><img class="uimg" src="../../assets/img/uimg/u__chat-img12.jpg" /><span class="name">孙莉</span></a></li>
                    <li><a routerLink="/contact/uinfo"><img class="uimg" src="../../assets/img/uimg/u__chat-img05.jpg" /><span class="name">赵小帅</span></a></li>
                    <li><a routerLink="/contact/uinfo"><img class="uimg" src="../../assets/img/uimg/u__chat-img08.jpg" /><span class="name">BABA</span></a></li>
                    <li><a routerLink="/contact/uinfo"><img class="uimg" src="../../assets/img/uimg/u__chat-img15.jpg" /><span class="name">一凡</span></a></li>
                    <li><a routerLink="/contact/uinfo"><img class="uimg" src="../../assets/img/uimg/u__chat-img10.jpg" /><span class="name">Apple</span></a></li>
                    <li><a routerLink="/contact/uinfo"><img class="uimg" src="../../assets/img/uimg/u__chat-img11.jpg" /><span class="name">Angle</span></a></li>
                    <li><a routerLink="/contact/uinfo"><img class="uimg" src="../../assets/img/uimg/u__chat-img06.jpg" /><span class="name">追风少年</span></a></li>
                    <li><a routerLink="/contact/uinfo"><img class="uimg" src="../../assets/img/uimg/u__chat-img04.jpg" /><span class="name">龙哥</span></a></li>
                    <li><a routerLink="/contact/uinfo"><img class="uimg" src="../../assets/img/uimg/u__chat-img07.jpg" /><span class="name">么么哒</span></a></li>
                </ul>
            </div>
            <div class="wc__ucinfo-qunInfo"> <ul class="clearfix"> <li>
                        <div class="item flexbox flex-alignc wcim__material-cell">
                            <label class="lbl flex1">群聊名称</label> <input class="val" type="text" value="Angular交流群" readOnly />
                        </div>
                        <div class="item flexbox flex-alignc wcim__material-cell"> <label class="lbl flex1">我在本群的昵称</label> <input class="val" type="text" value="王梅（Fine）" />
                        </div>
                    </li>
                    <li> <div class="item flexbox flex-alignc">
                            <label class="lbl flex1">置顶聊天</label> <div class="cnt"><input class="cp__checkbox-switch" type="checkbox" /></div>
                        </div>
                        <div class="item flexbox flex-alignc"> <label class="lbl flex1">显示群成员昵称</label>
                            <div class="cnt"><input class="cp__checkbox-switch" type="checkbox" defaultChecked /></div>
                        </div>
                    </li>
                    <li><div class="item flexbox flex-alignc wcim__material-cell">
                            <label class="lbl flex1">设置当前聊天背景</label> </div>
                    </li>
                    <li> <div class="item flexbox flex-alignc wcim__material-cell">
                            <label class="lbl flex1">清空聊天记录</label>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="wc__btns-panel"> <a class="wc__btn-primary" style="background:#ff3b30" (click)="quitGroupChat()">删除并退出</a>
            </div>
        </div>
    `,
    styles: [``]
})
export class GroupInfoComponent implements OnInit {
    constructor(
        private router: Router
    ) { }
    ngOnInit(): void { }
    quitGroupChat(){
        let that = this;
        let quitChat = wcPop({
            skin: 'android', content: '确认要退群吗，删除并退出后，将不再接收此群聊信息！',
            btns: [ {
                    text: '取消', onTap() {wcPop.close();}
                },
                { text: '确定', style: 'color:#ff3b30', onTap() {
                        that.router.navigate(['/index'])
                    }
                }
            ]
        });
    }

    routeBack(){
        history.go(-1)
    }
}
